package main

import (
	"flag"
	"github.com/taotetek/gogopher"
	"log"
	"net"
	"path/filepath"
)

const (
	ConHost = "localhost"
)

var (
	confPort       = flag.String("port", "7070", "port to listen on")
	confGopherRoot = flag.String("root", "./example/", "root directory for gopher server")
)

func init() {
	flag.Parse()
}

func gopherHandler(conn net.Conn) {
	buf := make([]byte, 4096)
	path, _ := filepath.Abs("./example")
	path = "file://" + path
	gd, _ := gogopher.NewGopherDir(path)

	for {
		n, err := conn.Read(buf)
		if err != nil || n == 0 {
			conn.Close()
			break
		}
		n, err = conn.Write(gd.ToResponse())
		if err != nil {
			conn.Close()
			break
		}
		conn.Close()
		break
	}
}

func server(host string, port string, done chan int) {
	l, err := net.Listen("tcp", host+":"+port)
	if err != nil {
		log.Fatal("%s", err)
	}
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Println(err)
			continue
		}
		go gopherHandler(conn)
	}
	done <- 1
}

func main() {
	done := make(chan int)
	go server(ConHost, *confPort, done)
	<-done
}
